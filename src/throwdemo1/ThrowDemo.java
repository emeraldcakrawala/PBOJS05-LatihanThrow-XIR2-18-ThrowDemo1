
package throwdemo1;

public class ThrowDemo {

    public static void main(String[] args){
    String input = "invalid input";
try {
    if (!input.equals("invalid input"))
    {
        System.out.println(input);
    }
    else
    {
        throw new RuntimeException("throw demo");
    }
            System.out.println("After throwing");
    }catch (RuntimeException e)
    {
            System.out.println("Exception caught here.");
            System.out.println(e);
    }

    }
}
